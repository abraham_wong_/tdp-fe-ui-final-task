import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='navbar flex'>
        <div className='navbar-logo'>
          <a href="/#"><b>EDTS</b> TDP Batch #2</a>
        </div>
        
        {/* Add another div to separate "logo" from links */}
        <div className='navbar-link'>
          <a href="/#projects">Projects</a>
          <a href="/#about">About</a>
          <a href="/#contact">Contact</a>
        </div>
      </div>

      {/* Header */}
      <header>
        <img src={banner} alt="Architecture" width={1500} height={800} />
        <div>
          <h1>EDTS</h1>
          <h1>Architects</h1>
        </div>
      </header>

      {/* Page content */}
      <div className="container">

        {/* Project Section */}

        {/* Workaround for compensating 56px height navbar */}
        <div id="projects" className='anchors'></div>
        <div>
          <h3 className='title'>Projects</h3>
        </div>
        
        <div className='grid'>
          <div className='project'>
            <div>Summer House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='project'>
            <div>Brick House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='project'>
            <div>Renovated</div>
            <img src={house4} alt="House" />
          </div>
          <div className='project'>
            <div>Barn House</div>
            <img src={house5} alt="House" />
          </div>
          <div className='project'>
            <div>Summer House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='project'>
            <div>Brick House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='project'>
            <div>Renovated</div>
            <img src={house5} alt="House" />
          </div>
          <div className='project'>
            <div>Barn House</div>
            <img src={house4} alt="House" />
          </div>                
        </div>

        {/* About Section */}

        {/* Workaround for compensating 56px height navbar */}
        <div id="about" className='anchors'></div>
        
        <div>
          <h3 className='title'>About</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        
        {/* Add id author-section to separate authors from description */}
        {/* Add class authors to group image, heading, and author description 
            and class author-department to change the text color
        */}
        <div id='author-section'>
          <div className='authors'>
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p className='author-department'>CEO &amp; Founder</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='authors'>
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p className='author-department'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='authors'>
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p className='author-department'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='authors'>
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p className='author-department'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}

        {/* Workaround for compensating 56px height navbar */}
        <div id="contact" className='anchors'></div>

        <div>
          <h3 className='title'>Contact</h3>
          <p>Lets get in touch and talk about your next project.</p>

          {/***  
           add your element form here 
           ***/}
          <form className='flex-column'>
            <input type={"text"} placeholder={"Name"} name={"username"} />
            <input type={"email"} placeholder={"Email"} name={"email"} />
            <input type={"text"} placeholder={"Subject"} name={"subject"} />
            <input type={"text"} placeholder={"Comment"} name={"comment"} />
            <input type={"submit"} value={"SEND MESSAGE"}/>
          </form>

        </div>

        {/* Image of location/map */}
        <div id='location'>
          <img src={map} alt="maps" />
        </div>

        <div>
          <h3 className='title'>Contact List</h3>

          {/***  
           add your element table here 
           ***/}
          <table>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Country</th>
            </tr>
            <tr>
              <td>Alfreds Futterkiste</td>
              <td>alfreds@gmail.com</td>
              <td>Germany</td>
            </tr>
            <tr>
              <td>Reza</td>
              <td>reza@gmail.com</td>
              <td>Indonesia</td>
            </tr>
            <tr>
              <td>Ismail</td>
              <td>ismail@gmail.com</td>
              <td>Turkey</td>
            </tr>
            <tr>
              <td>Holand</td>
              <td>holand@gmail.com</td>
              <td>Belanda</td>
            </tr>
          </table>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
